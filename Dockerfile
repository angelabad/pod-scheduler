FROM golang:1.19 as build

WORKDIR /go/src/app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY main.go ./
COPY pkg ./pkg
COPY cmd ./cmd

#RUN go vet -v
#RUN go test -v

RUN CGO_ENABLED=0 go build -o /go/bin/pod-scheduler

FROM gcr.io/distroless/static-debian11

COPY --from=build /go/bin/pod-scheduler /
CMD ["/pod-scheduler"]
