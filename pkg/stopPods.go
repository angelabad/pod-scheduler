package scheduler

import (
	"context"
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/util/retry"
)

func StopPods(kubeconfig string, annotation string) {
	scheduler := newScheduler(kubeconfig)

	deploymentsClient := scheduler.clientset.AppsV1().Deployments("")
	list, err := deploymentsClient.List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	for _, d := range list.Items {
		if metav1.HasAnnotation(d.ObjectMeta, annotation) {

			retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
				result, getErr := scheduler.clientset.AppsV1().Deployments(d.Namespace).Get(context.TODO(), d.Name, metav1.GetOptions{})
				if getErr != nil {
					panic(fmt.Errorf("Failed to get latest version of deployment: %v", getErr))
				}

				ann := result.ObjectMeta.GetAnnotations()
				if ann == nil {
					fmt.Println("No hay annotaciones")
					ann = make(map[string]string)
				}
				if *result.Spec.Replicas != 0 {
					ann["pod-scheduler.angelabad.me/lastInstanceCount"] = fmt.Sprint(*result.Spec.Replicas)
				}

				result.Spec.Replicas = int32Ptr(0)
				result.ObjectMeta.SetAnnotations(ann)
				fmt.Printf("Scaling: Pod: %s - Namespace: %s - From: %d - To: %d\n", d.Name, d.Namespace, *d.Spec.Replicas, 0)

				_, updateErr := scheduler.clientset.AppsV1().Deployments(d.Namespace).Update(context.TODO(), result, metav1.UpdateOptions{})
				return updateErr
			})
			if retryErr != nil {
				panic(fmt.Errorf("Update failed: %v", retryErr))
			}

		}
	}
}
