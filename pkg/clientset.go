package scheduler

import (
	"os"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/klog/v2"
)

type scheduler struct {
	clientset kubernetes.Clientset
}

func int32Ptr(i int32) *int32 { return &i }

func newScheduler(kubeconfig string) scheduler {
	config, err := rest.InClusterConfig()
	if err != nil {
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
		if err != nil {
			klog.Error(err)
			os.Exit(1)
		}
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	scheduler := scheduler{
		clientset: *clientset,
	}

	return scheduler
}
