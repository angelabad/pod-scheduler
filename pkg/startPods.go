package scheduler

import (
	"context"
	"fmt"
	"strconv"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/util/retry"
)

func StartPods(kubeconfig string, annotation string) {
	scheduler := newScheduler(kubeconfig)

	deploymentsClient := scheduler.clientset.AppsV1().Deployments("")
	list, err := deploymentsClient.List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	for _, d := range list.Items {
		if metav1.HasAnnotation(d.ObjectMeta, annotation) && metav1.HasAnnotation(d.ObjectMeta, "pod-scheduler.angelabad.me/lastInstanceCount") {
			retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
				result, getErr := scheduler.clientset.AppsV1().Deployments(d.Namespace).Get(context.TODO(), d.Name, metav1.GetOptions{})
				if getErr != nil {
					panic(fmt.Errorf("Failed to get latest version of deployment: %v", getErr))
				}

				ann := result.ObjectMeta.GetAnnotations()
				replicaCount := ann["pod-scheduler.angelabad.me/lastInstanceCount"]
				fmt.Printf("Scaling %s to %s\n", d.Name, replicaCount)

				i, _ := strconv.ParseInt(replicaCount, 10, 32)
				result.Spec.Replicas = int32Ptr(int32(i))
				result.ObjectMeta.SetAnnotations(ann)
				fmt.Printf("Scaling: Pod: %s - Namespace: %s - From: %d - To: %d\n", d.Name, d.Namespace, 0, *result.Spec.Replicas)

				_, updateErr := scheduler.clientset.AppsV1().Deployments(d.Namespace).Update(context.TODO(), result, metav1.UpdateOptions{})

				return updateErr
			})
			if retryErr != nil {
				panic(fmt.Errorf("Update failed: %v", retryErr))
			}
		}
	}
}
