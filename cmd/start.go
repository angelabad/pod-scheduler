/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	scheduler "angelabad.me/pod-scheduler/pkg"
	"github.com/spf13/cobra"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Start scheduled deployments",
	Long:  `Restore replicas on scheduled deployments.`,
	Run: func(cmd *cobra.Command, args []string) {
		scheduler.StartPods(kubeconfig, annotation)
	},
}

func init() {
	rootCmd.AddCommand(startCmd)
}
