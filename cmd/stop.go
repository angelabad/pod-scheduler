/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	scheduler "angelabad.me/pod-scheduler/pkg"
	"github.com/spf13/cobra"
)

// stopCmd represents the stop command
var stopCmd = &cobra.Command{
	Use:   "stop",
	Short: "Stop scheduled deployments",
	Long:  `Set replicas to 0 on scheduled deployments and save current state.`,
	Run: func(cmd *cobra.Command, args []string) {
		scheduler.StopPods(kubeconfig, annotation)
	},
}

func init() {
	rootCmd.AddCommand(stopCmd)
}
