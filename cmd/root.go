/*
Copyright © 2022 Angel Abad <me@angelabad.me>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"k8s.io/client-go/util/homedir"
)

var kubeconfig string
var annotation string

var rootCmd = &cobra.Command{
	Use:   "pod-scheduler",
	Short: "Kubernetes pod start/stop scheduler",
	Long:  `Kubernetes job for start and stop jobs with schedule.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	defaultConfig := ""
	if home := homedir.HomeDir(); home != "" {
		defaultConfig = filepath.Join(home, ".kube", "config")
	}

	rootCmd.PersistentFlags().StringVar(&kubeconfig, "kubeconfig", defaultConfig, "absolute path to the config file")
	rootCmd.PersistentFlags().StringVar(&annotation, "annotation", "pod-scheduler.angelabad.me/enabled", "Annotation you should define in your deployments")
}
